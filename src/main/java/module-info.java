module batoipop {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires com.google.gson;


    opens batoipop to javafx.fxml;
    exports batoipop.vista;
    opens batoipop.vista to javafx.fxml;
    exports batoipop;
}
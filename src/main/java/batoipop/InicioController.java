package batoipop;

import batoipop.dao.BatoipopUsuarioDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class InicioController implements Initializable {

    @FXML
    private AnchorPane bg;

    @FXML
    private Button btnlogin;

    @FXML
    private TextField inputEmail;

    @FXML
    private PasswordField inputPass;

    @FXML
    private VBox loginInputs;

    @FXML
    private ImageView icon1;

    @FXML
    private ImageView logo;

    @FXML
    private ImageView icon2;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        icon1.setImage(new Image("file:img/IconoUsuario.png"));
        icon2.setImage(new Image("file:img/IconoCandado.png"));
        logo.setImage(new Image("file:img/batoilogo.png"));
    }

    @FXML
    void logIn(ActionEvent event) throws IOException {
        try {
            BatoipopUsuarioDAO userDAO = new BatoipopUsuarioDAO();
            int userID = userDAO.canLogin(inputEmail.getText(),inputPass.getText());
            System.out.println(userID);
            if (userID  == 0){
                inputEmail.setText("");
                inputPass.setText("");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error de credenciales");
                alert.setHeaderText("Empleado no encontrado");
                alert.setContentText("El usuario o contraseña no coinciden con ningún registro");

                alert.showAndWait();
            }else {
                Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
                Scene scene = new Scene(blah);
                Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage.setScene(scene);
                stage.setTitle("BatoiPop");
                stage.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
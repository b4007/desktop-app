package batoipop;

import batoipop.dao.ArticuloDAO;
import batoipop.dao.DenunciaUsuarioDAO;
import batoipop.dao.EtiquetaDAO;
import batoipop.dao.UsuarioDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

public class GesDenunciasController implements Initializable {

    @FXML
    private ImageView imgBack;
    @FXML
    private TextField busqueda, busqueda1;

    @FXML
    private TableView<DenunciaUsuario> tblDenuncia;
    @FXML
    private TableColumn<DenunciaUsuario, Integer> clmnDenunciado;
    @FXML
    private TableColumn<DenunciaUsuario, Integer> clmnDenunciante;

    private DenunciaUsuarioDAO denunciaUsuario = new DenunciaUsuarioDAO();
    private UsuarioDAO usuario = new UsuarioDAO();

    ObservableList<DenunciaUsuario> obList = FXCollections.observableArrayList();

    private DenunciaUsuarioDAO tag = new DenunciaUsuarioDAO();

    private FilteredList<DenunciaUsuario> filteredData;
    private SortedList<DenunciaUsuario> sortedData;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image imageBack = new Image("file:img/backarrow.png");
        imgBack.setImage(imageBack);

        try {
            visualizarTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }

        filterDenunciado();
        filterDenunciante();

    }

    @FXML
    void goBack(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    public void visualizarTablas() throws Exception {

        List<DenunciaUsuario> registro = new ArrayList<>();
        registro = denunciaUsuario.findAll();

        for (int i = 0; i < registro.size(); i++) {
            registro.get(i).setNombreDenunciado(usuario.findByPK(registro.get(i).getDenunciado()).getNickname());
            registro.get(i).setNombreDenunciante(usuario.findByPK(registro.get(i).getDenunciante()).getNickname());
        }

        obList = FXCollections.observableArrayList(registro);

        if(obList.size() != 0){
            tblDenuncia.setItems(obList);
            tblDenuncia.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            clmnDenunciante.setCellValueFactory(new PropertyValueFactory<>("nombreDenunciante"));
            clmnDenunciado.setCellValueFactory(new PropertyValueFactory<>("nombreDenunciado"));
        }
    }

    @FXML
    private void rmvUsuario(ActionEvent event) {
        try {
            String mensaje = "¿Estás seguro de eliminarlo?" ;

            try {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, mensaje);
                alert.setTitle("Cuidado!");
                alert.setHeaderText(mensaje);
                alert.setContentText("");
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK){
                    try {
                        tag.deleteUsuarioDenunciado(tblDenuncia.getSelectionModel().getSelectedItem().getDenunciado());
                        for(int i = 0; i < obList.size(); i++){
                            if(Objects.equals(obList.get(i).getNombreDenunciado(), tblDenuncia.getSelectionModel().getSelectedItem().getNombreDenunciado())){
                                obList.remove(obList.get(i));
                            }
                            tblDenuncia.refresh();
                        }
                        tblDenuncia.setItems(obList);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    @FXML
    private void rmvDenuncia(ActionEvent event) {
        try {
            String mensaje = "¿Estás seguro de eliminarlo?" ;

            try {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, mensaje);
                alert.setTitle("Cuidado!");
                alert.setHeaderText(mensaje);
                alert.setContentText("");
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK){
                    try {
                        tag.delete(tblDenuncia.getSelectionModel().getSelectedItem());
                        obList.removeAll(tblDenuncia.getSelectionModel().getSelectedItems());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void filterDenunciado(){
        filteredData = new FilteredList<>(obList, b -> true);

        busqueda1.textProperty().addListener((observable, oldValue, newValue) ->{
            busqueda.setDisable(!newValue.isEmpty());
            filteredData.setPredicate(denunciado -> {
                if(newValue.isEmpty()){
                    return true;
                }
                String lowerCase = newValue.toLowerCase();

                if(denunciado.getNombreDenunciado().toLowerCase().contains(lowerCase)){
                    return true;
                }else{
                    return false;
                }
            });
        });

        sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tblDenuncia.comparatorProperty());

        tblDenuncia.setItems(sortedData);

        tblDenuncia.refresh();
    }


    private void filterDenunciante(){
        filteredData = new FilteredList<>(obList, b -> true);

        busqueda.textProperty().addListener((observable, oldValue, newValue) ->{
            busqueda1.setDisable(!newValue.isEmpty());
            filteredData.setPredicate(denunciante -> {
                if(newValue.isEmpty()){
                    return true;
                }
                String lowerCase = newValue.toLowerCase();

                if(denunciante.getNombreDenunciante().toLowerCase().contains(lowerCase)){
                    return true;
                }else{
                    return false;
                }
            });
        });

        sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tblDenuncia.comparatorProperty());

        tblDenuncia.setItems(sortedData);

        tblDenuncia.refresh();
    }



}

package batoipop;


import batoipop.dao.UsuarioDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class GesUsuariosController implements Initializable {

    @FXML
    private ImageView imgBack;
    @FXML
    private TextField busqueda;
    @FXML
    private ImageView userImg;
    @FXML
    private TableView<Usuario> tblUsuarios;
    @FXML
    private TableColumn<Usuario, Integer> clmnId;
    @FXML
    private TableColumn<Usuario, String> clmnNickname;

    private UsuarioDAO usuario = new UsuarioDAO();

    ObservableList<Usuario> obList = FXCollections.observableArrayList();

    private UsuarioDAO tag = new UsuarioDAO();


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image imageBack = new Image("file:img/backarrow.png");
        imgBack.setImage(imageBack);
        userImg.setImage(new Image("file:img/batoilogo.png"));
//        userImg.setVisible(false);

        try {
            visualizarTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }

        filter();

    }

    @FXML
    void goBack(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    public void visualizarTablas() throws Exception {
        List<Usuario> registro = new ArrayList<>();
        registro = usuario.findAll();


        obList = FXCollections.observableArrayList(registro);

        if(obList.size() != 0){
            tblUsuarios.setItems(obList);
            tblUsuarios.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            clmnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            clmnNickname.setCellValueFactory(new PropertyValueFactory<>("nickname"));
        }
    }

    @FXML
    private void removeItem(ActionEvent event) {
        try {
            String mensaje = "¿Estás seguro de eliminarlo?" ;

            try {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, mensaje);
                alert.setTitle("Cuidado!");
                alert.setHeaderText(mensaje);
                alert.setContentText("");
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK){
                    try {
                        tag.delete(tblUsuarios.getSelectionModel().getSelectedItem());
                        obList.removeAll(tblUsuarios.getSelectionModel().getSelectedItems());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void filter(){
        FilteredList<Usuario> filteredData = new FilteredList<>(obList, b -> true);

        busqueda.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredData.setPredicate(usuario1 -> {
                if(newValue.isEmpty()){
                    return true;
                }
                String lowerCase = newValue.toLowerCase();

                if(usuario1.getNickname().toLowerCase().indexOf(lowerCase) != -1){
                    return true;
                }else{
                    return false;
                }
            });
        });

        SortedList<Usuario> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tblUsuarios.comparatorProperty());

        tblUsuarios.setItems(sortedData);

        tblUsuarios.refresh();
    }

//    @FXML
//    private void updateItem(ActionEvent event) {
//        try {
//            String newName = "";
//            TextInputDialog dialog = new TextInputDialog("");
//            dialog.setTitle("Modificar nombre");
//            dialog.setHeaderText(null);
//            dialog.setContentText("Escribe el nuevo nombre del usuario:");
//            Optional<String> result = dialog.showAndWait();
//            if (result.isPresent()){
//                newName = result.get();
//            }
//
//            Usuario et = new Usuario(tblUsuarios.getSelectionModel().getSelectedItem().getId(),newName);
//            tag.update(et);
//            int indice = tblUsuarios.getSelectionModel().getSelectedIndex();
//            Usuario item = tag.findByPK(tblUsuarios.getSelectionModel().getSelectedItem().getId());
//            obList.set(indice, item);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}

package batoipop;

public class DenunciaUsuario {

    public int id;
    public int denunciado;
    public int denunciante;
    public String nombreDenunciante;
    public String nombreDenunciado;

    public DenunciaUsuario() {
    }

    public DenunciaUsuario(int id, int denunciado, int denunciante) {
        this.id = id;
        this.denunciado = denunciado;
        this.denunciante = denunciante;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getDenunciado() {
        return denunciado;
    }
    public void setDenunciado(int denunciado) {
        this.denunciado = denunciado;
    }

    public int getDenunciante() {
        return denunciante;
    }
    public void setDenunciante(int denunciante) {
        this.denunciante = denunciante;
    }

    public String getNombreDenunciante() {
        return nombreDenunciante;
    }
    public void setNombreDenunciante(String nombreDenunciante) {
        this.nombreDenunciante = nombreDenunciante;
    }

    public String getNombreDenunciado() {
        return nombreDenunciado;
    }
    public void setNombreDenunciado(String nombreDenunciado) {
        this.nombreDenunciado = nombreDenunciado;
    }

    @Override
    public String toString() {
        return "DenunciaUsuario[id= " + id +
                ", denunciado= " + denunciado +
                ", denunciante= " + denunciante + "]";
    }
}

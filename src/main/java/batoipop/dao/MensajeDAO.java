package batoipop.dao;

import batoipop.Mensaje;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MensajeDAO implements GenericDAO<Mensaje>{

    @Override
    public Mensaje findByPK(int id) throws Exception {
        String lin, salida = "";
        Mensaje mensaje = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/mensaje/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }
            gson = new Gson();
            mensaje = gson.fromJson(salida, Mensaje.class);
        }
        con.disconnect();

        return mensaje;
    }

    @Override
    public List<Mensaje> findAll() throws Exception {
        String lin, salida = "";
        Mensaje[] mensajes;
        List<Mensaje> listMensajes = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/mensaje");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }

            gson = new Gson();
            mensajes = gson.fromJson(salida, Mensaje[].class);
            listMensajes = new ArrayList<>(Arrays.asList(mensajes));

        }
        con.disconnect();

        return listMensajes;
    }

    @Override
    public boolean insert(Mensaje t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, Mensaje.class);
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/mensaje");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            delete = false;
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public Mensaje insertGenKey(Mensaje t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String texto;
        String convert = gson.toJson(t, Mensaje.class);

        url = new URL("http://137.74.226.41:8080/mensaje");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        texto = br.readLine();
        con.disconnect();
        t.setId(Integer.parseInt(texto));

        return t;
    }

    @Override
    public boolean update(Mensaje t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, Mensaje.class);
        boolean delete = true;


        url = new URL("http://137.74.226.41:8080/mensaje/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/mensaje/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(Mensaje t) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/empleado/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public int size() throws Exception {
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return false;
    }

    @Override
    public List<Mensaje> findByExample(Mensaje t) throws Exception {
        return null;
    }
}
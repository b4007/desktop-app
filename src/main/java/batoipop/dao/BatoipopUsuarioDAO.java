package batoipop.dao;


import batoipop.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


public class BatoipopUsuarioDAO implements GenericDAO<Usuario> {

    final String SQLSELECTALL = "SELECT * FROM articulos";
    final String SQLSELECTNOMBRE = "SELECT * FROM batoipop_empleado WHERE nickname = ? OR email = ?";
    final String SQLSELECTPK = "SELECT * FROM batoipop_usuario WHERE email = ? AND contraseña = ?";
    final  String SQLOGIN = "SELECT * FROM batoipop_empleado WHERE email = ? AND contraseña = ?";
    final  String SQLREGISTER = "SELECT * FROM batoipop_usuario WHERE email = ? OR nickname = ?";
    final String SQLINSERT = "INSERT INTO batoipop_articulo (categoria_id, vendedor, nombre, precio, antigüedad, descripcion) VALUES (?,?,?,?,?,?)";
    final String SQLUPDATE = "UPDATE articulos SET nombre = ?, precio = ? WHERE id = ?";
    final String SQLDELETE = "DELETE FROM articulos WHERE id = ?";

    private final PreparedStatement pstSelectPK;
    private final PreparedStatement pstSelectNombre;
    private final PreparedStatement pstSelectLogin;
    private final PreparedStatement pstSelectRegister;
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstInsertGenKey;
    private final PreparedStatement pstUpdate;
    private final PreparedStatement pstDelete;




    public BatoipopUsuarioDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
        pstSelectNombre = con.prepareStatement(SQLSELECTNOMBRE);
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstSelectLogin = con.prepareStatement(SQLOGIN);
        pstSelectRegister = con.prepareStatement(SQLREGISTER);
        pstInsert = con.prepareStatement(SQLINSERT);
        pstInsertGenKey = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstUpdate = con.prepareStatement(SQLUPDATE);
        pstDelete = con.prepareStatement(SQLDELETE);
    }

    public void cerrar() throws SQLException {
//        pstSelectPK.close();
//        pstSelectAll.close();
//        pstInsert.close();
//        pstUpdate.close();
//        pstDelete.close();
//        pstSelectLogin.close();
//        pstSelectRegister.close();
    }

    public int canLogin(String email, String pass) throws SQLException{
        pstSelectLogin.setString(1, email);
        pstSelectLogin.setString(2, pass);
        ResultSet rs = pstSelectLogin.executeQuery();
        if (rs.next()){
            return rs.getInt("id");
        }
        rs.close();
        return 0;
    }

    public boolean canRegister(Usuario user) throws SQLException{
        pstSelectRegister.setString(1, user.getEmail());
        pstSelectRegister.setString(2, user.getNickname());
        ResultSet rs = pstSelectRegister.executeQuery();
        if (rs.next()){
            rs.close();
            return false;
        }
        rs.close();
        return true;
    }

    @Override
    public Usuario findByPK(int id) throws SQLException {
        return null;
    }

    public Usuario findByNombre(String nombre) throws SQLException {
        return null;
    }

    @Override
    public List<Usuario> findAll() throws SQLException {
        return null;
    }

    @Override
    public boolean insert(Usuario artInsertar) throws SQLException {
        return false;
    }

    public Usuario insertGenKey(Usuario artInsertar) throws SQLException{
       return null;
    }

    @Override
    public boolean update(Usuario artUpdate) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        return false;
    }

    @Override
    public boolean delete(Usuario artDelete) throws SQLException {
        return this.delete(artDelete.getId());
    }

    @Override
    public int size() throws SQLException {
        return 0;
    }

    @Override
    public boolean exists(int id) throws SQLException {
        if (findByPK(id) != null){
			return true;
		}
		return false;
    }

    @Override
    public List<Usuario> findByExample(Usuario t) throws Exception {
        return null;
    }


}

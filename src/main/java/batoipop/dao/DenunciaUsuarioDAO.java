package batoipop.dao;

import batoipop.DenunciaUsuario;
import batoipop.Usuario;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DenunciaUsuarioDAO implements GenericDAO<DenunciaUsuario>{

    @Override
    public DenunciaUsuario findByPK(int id) throws Exception {
        String lin, salida = "";
        DenunciaUsuario denunciaUsuario = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/denunciausuario/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }
            gson = new Gson();
            denunciaUsuario = gson.fromJson(salida, DenunciaUsuario.class);
        }
        con.disconnect();

        return denunciaUsuario;
    }

    @Override
    public List<DenunciaUsuario> findAll() throws Exception {
        String lin, salida = "";
        DenunciaUsuario[] denunciaUsuarios;
        List<DenunciaUsuario> listDenunciaUsuarios = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/denunciausuario");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }

            gson = new Gson();
            denunciaUsuarios = gson.fromJson(salida, DenunciaUsuario[].class);
            listDenunciaUsuarios = new ArrayList<>(Arrays.asList(denunciaUsuarios));

        }
        con.disconnect();

        return listDenunciaUsuarios;
    }

    @Override
    public boolean insert(DenunciaUsuario t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, DenunciaUsuario.class);
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/denunciausuario");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            delete = false;
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public DenunciaUsuario insertGenKey(DenunciaUsuario t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String texto;
        String convert = gson.toJson(t, DenunciaUsuario.class);

        url = new URL("http://137.74.226.41:8080/denunciausuario");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        texto = br.readLine();
        con.disconnect();
        t.setId(Integer.parseInt(texto));

        return t;
    }

    @Override
    public boolean update(DenunciaUsuario t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, DenunciaUsuario.class);
        boolean delete = true;


        url = new URL("http://137.74.226.41:8080/denunciausuario/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/denunciausuario/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(DenunciaUsuario t) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/denunciausuario/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    public boolean deleteUsuarioDenunciado(int u) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/usuario/" + u);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public int size() throws Exception {
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return false;
    }

    @Override
    public List<DenunciaUsuario> findByExample(DenunciaUsuario t) throws Exception {
        return null;
    }
}


package batoipop.dao;

import batoipop.Etiqueta;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EtiquetaDAO implements GenericDAO<Etiqueta>{

    @Override
    public Etiqueta findByPK(int id) throws Exception {
        String lin, salida = "";
        Etiqueta etiqueta = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/etiqueta/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }
            gson = new Gson();
            etiqueta = gson.fromJson(salida, Etiqueta.class);
        }
        con.disconnect();

        return etiqueta;
    }

    @Override
    public List<Etiqueta> findAll() throws Exception {
        String lin, salida = "";
        Etiqueta[] etiquetas;
        List<Etiqueta> listEtiquetas = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        url = new URL("http://137.74.226.41:8080/etiqueta");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            while ((lin = br.readLine()) != null) {
                salida = salida.concat(lin);
            }

            gson = new Gson();
            etiquetas = gson.fromJson(salida, Etiqueta[].class);
            listEtiquetas = new ArrayList<>(Arrays.asList(etiquetas));

        }
        con.disconnect();

        return listEtiquetas;
    }

    @Override
    public boolean insert(Etiqueta t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, Etiqueta.class);
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/etiqueta");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            delete = false;
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public Etiqueta insertGenKey(Etiqueta t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String texto;
        String convert = gson.toJson(t, Etiqueta.class);

        url = new URL("http://137.74.226.41:8080/etiqueta");
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
        texto = br.readLine();
        con.disconnect();
        t.setId(Integer.parseInt(texto));

        return t;
    }

    @Override
    public boolean update(Etiqueta t) throws Exception {
        URL url;
        HttpURLConnection con;
        Gson gson = new Gson();
        String convert = gson.toJson(t, Etiqueta.class);
        boolean delete = true;


        url = new URL("http://137.74.226.41:8080/etiqueta/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("PUT");
        con.setRequestProperty("Content-Type", "application/json");

        OutputStream os = con.getOutputStream();
        os.write(convert.getBytes());
        os.flush();

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/etiqueta/" + id);
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public boolean delete(Etiqueta t) throws Exception {
        URL url;
        HttpURLConnection con;
        boolean delete = true;

        url = new URL("http://137.74.226.41:8080/etiqueta/" + t.getId());
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("DELETE");

        if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
        }
        con.disconnect();

        return delete;
    }

    @Override
    public int size() throws Exception {
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return false;
    }

    @Override
    public List<Etiqueta> findByExample(Etiqueta t) throws Exception {
        return null;
    }
}


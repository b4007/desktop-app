package batoipop;

import batoipop.dao.EtiquetaDAO;
import batoipop.dao.UsuarioDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class GesEtiquetasController implements Initializable {

    @FXML
    private ImageView imgBack;
    @FXML
    private TextField busqueda;

    @FXML
    private TableView<Etiqueta> tblTags;
    @FXML
    private TableColumn<Etiqueta, Integer> clmnId;
    @FXML
    private TableColumn<Etiqueta, String> clmnNombre;

    private EtiquetaDAO tag = new EtiquetaDAO();

    ObservableList<Etiqueta> obList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image imageBack = new Image("file:img/backarrow.png");
        imgBack.setImage(imageBack);

        try {
            visualizarTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }

        filter();
    }

    @FXML
    void goBack(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    public void visualizarTablas() throws Exception {

        List<Etiqueta> registro = new ArrayList<>();
        registro = tag.findAll();


        obList = FXCollections.observableArrayList(registro);

        if(obList.size() != 0){
            tblTags.setItems(obList);
            tblTags.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            clmnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            clmnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        }
    }

    @FXML
    private void removeItem(ActionEvent event) {

        try {
            String mensaje = "¿Estás seguro de eliminarlo?" ;

                try {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, mensaje);
                    alert.setTitle("Cuidado!");
                    alert.setHeaderText(mensaje);
                    alert.setContentText("");
                    alert.showAndWait();
                    if(alert.getResult() == ButtonType.OK){
                        try {
                            tag.delete(tblTags.getSelectionModel().getSelectedItem());
                            obList.removeAll(tblTags.getSelectionModel().getSelectedItems());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @FXML
    private void updateItem(ActionEvent event) {
        try {
            String newName = "";
            TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Modificar nombre");
            dialog.setHeaderText(null);
            dialog.setContentText("Escribe el nombre de la nueva etiqueta:");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){
                newName = result.get();
            }

            Etiqueta et = new Etiqueta(tblTags.getSelectionModel().getSelectedItem().getId(),newName);
            tag.update(et);
            int indice = tblTags.getSelectionModel().getSelectedIndex();
            Etiqueta item = tag.findByPK(tblTags.getSelectionModel().getSelectedItem().getId());
            obList.set(indice, item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void filter(){
        FilteredList<Etiqueta> filteredData = new FilteredList<>(obList, b -> true);

        busqueda.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredData.setPredicate(etiqueta1 -> {
                if(newValue.isEmpty()){
                    return true;
                }
                String lowerCase = newValue.toLowerCase();

                if(etiqueta1.getNombre().toLowerCase().indexOf(lowerCase) != -1){
                    return true;
                }else{
                    return false;
                }
            });
        });

        SortedList<Etiqueta> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tblTags.comparatorProperty());

        tblTags.setItems(sortedData);

        tblTags.refresh();
    }


}


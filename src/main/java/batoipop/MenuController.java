package batoipop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {

    @FXML
    private ImageView centerImage;


    @FXML
    void Opcion(ActionEvent event) throws IOException {
        String opcion = ((Button) event.getSource()).getText();

        switch(opcion) {
            case "Gestionar categorías":
                Parent blah1 = FXMLLoader.load(getClass().getResource("gestion_categorias.fxml"));
                Scene scene1 = new Scene(blah1);
                Stage stage1 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage1.setScene(scene1);
                stage1.setTitle("BatoiPop");
                stage1.show();
                break;
            case "Gestionar etiquetas":
                Parent blah2 = FXMLLoader.load(getClass().getResource("gestion_etiquetas.fxml"));
                Scene scene2 = new Scene(blah2);
                Stage stage2 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage2.setScene(scene2);
                stage2.setTitle("BatoiPop");
                stage2.show();
                break;
            case "Gestionar articulos":
                Parent blah3 = FXMLLoader.load(getClass().getResource("gestion_articulos.fxml"));
                Scene scene3 = new Scene(blah3);
                Stage stage3 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage3.setScene(scene3);
                stage3.setTitle("BatoiPop");
                stage3.show();
                break;
            case "Gestionar usuarios":
                Parent blah4 = FXMLLoader.load(getClass().getResource("gestion_usuarios.fxml"));
                Scene scene4 = new Scene(blah4);
                Stage stage4 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage4.setScene(scene4);
                stage4.setTitle("BatoiPop");
                stage4.show();
                break;
            case "Gestionar denuncias":
                Parent blah5 = FXMLLoader.load(getClass().getResource("gestion_denuncias.fxml"));
                Scene scene5 = new Scene(blah5);
                Stage stage5 = (Stage) ((Node) event.getSource()).getScene().getWindow();
                stage5.setScene(scene5);
                stage5.setTitle("BatoiPop");
                stage5.show();
                break;
        }
    }

    @FXML
    void CerrarSesion(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("inicio.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        centerImage.setImage(new Image("file:img/batoilogo.png"));
    }
}

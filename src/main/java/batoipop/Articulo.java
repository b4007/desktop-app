package batoipop;

public class Articulo {

    private int id;
    private String nombre;
    private float precio;
    private int batoipopUsuarioByComprador;
    private int batoipopUsuarioByVendedor;
    private int batoipopCategoria;
    private String antiguedad;
    private String descripcion;
    private String nombreVendedor;

    public Articulo() {
    }

    public Articulo(int id, String nombre, float precio, int batoipopUsuarioByComprador, int batoipopUsuarioByVendedor, int batoipopCategoria, String antiguedad, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.batoipopUsuarioByComprador = batoipopUsuarioByComprador;
        this.batoipopUsuarioByVendedor = batoipopUsuarioByVendedor;
        this.batoipopCategoria = batoipopCategoria;
        this.antiguedad = antiguedad;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getComprador() {
        return batoipopUsuarioByComprador;
    }
    public void setComprador(int comprador) {
        this.batoipopUsuarioByComprador = comprador;
    }

    public int getVendedor() {
        return batoipopUsuarioByVendedor;
    }
    public void setVendedor(int vendedor) {
        this.batoipopUsuarioByVendedor = vendedor;
    }

    public int getCategoriaId() {
        return batoipopCategoria;
    }
    public void setCategoriaId(int categoriaId) {
        this.batoipopCategoria = categoriaId;
    }

    public String getAntiguedad() {
        return antiguedad;
    }
    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    @Override
    public String toString() {
        return "Articulo [id= " + id +
                ", nombre= " + nombre +
                ", precio= " + precio +
                ", comprador= " + batoipopUsuarioByComprador +
                ", vendedor= " + batoipopUsuarioByVendedor +
                ", categoriaId= " + batoipopCategoria +
                ", antiguedad= " + antiguedad +
                ", descripcion= " + descripcion + "]";
    }


}

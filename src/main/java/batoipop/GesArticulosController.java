package batoipop;

import batoipop.dao.ArticuloDAO;
import batoipop.dao.EtiquetaDAO;
import batoipop.dao.UsuarioDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GesArticulosController implements Initializable {

    @FXML
    private ImageView imgBack;
    @FXML
    private TextField busqueda;
    @FXML
    private ImageView artImg;
    @FXML
    private TableView<Articulo> tblArticulos;
    @FXML
    private TableColumn<Articulo, String> clmnArticulo;
    @FXML
    private TableColumn<Articulo, String> clmnUsuario;
    @FXML
    private TableColumn<Articulo, String> clmnDescripcion;

    private ArticuloDAO articulo = new ArticuloDAO();
    private UsuarioDAO usuario = new UsuarioDAO();

    private ArticuloDAO tag = new ArticuloDAO();

    ObservableList<Articulo> obList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image imageBack = new Image("file:img/backarrow.png");
        imgBack.setImage(imageBack);
        artImg.setImage(new Image("file:img/batoilogo.png"));
        try {
            visualizarTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }

        filter();
    }

    @FXML
    void goBack(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    public void visualizarTablas() throws Exception {

        List<Articulo> registro = new ArrayList<>();
        registro = articulo.findAll();

        for (int i = 0; i < registro.size(); i++) {
            registro.get(i).setNombreVendedor(usuario.findByPK(registro.get(i).getVendedor()).getNickname());
        }

        obList = FXCollections.observableArrayList(registro);

        if(obList.size() != 0){
            tblArticulos.setItems(obList);
            tblArticulos.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            clmnArticulo.setCellValueFactory(new PropertyValueFactory<>("nombre"));
            clmnUsuario.setCellValueFactory(new PropertyValueFactory<>("nombreVendedor"));
            clmnDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));

        }
    }

    @FXML
    public void rmvArticulos(){
        try {
            String mensaje = "¿Estás seguro de eliminarlo?" ;

            try {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, mensaje);
                alert.setTitle("Cuidado!");
                alert.setHeaderText(mensaje);
                alert.setContentText("");
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK){
                    try {
                        tag.delete(tblArticulos.getSelectionModel().getSelectedItem());
                        obList.removeAll(tblArticulos.getSelectionModel().getSelectedItems());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void filter(){
        FilteredList<Articulo> filteredData = new FilteredList<>(obList, b -> true);

        busqueda.textProperty().addListener((observable, oldValue, newValue) ->{
            filteredData.setPredicate(articulo1 -> {
                if(newValue.isEmpty()){
                    return true;
                }
                String lowerCase = newValue.toLowerCase();

                if(articulo1.getNombre().toLowerCase().indexOf(lowerCase) != -1){
                    return true;
                }else{
                    return false;
                }
            });
        });

        SortedList<Articulo> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tblArticulos.comparatorProperty());

        tblArticulos.setItems(sortedData);

        tblArticulos.refresh();
    }

}

package batoipop;

import batoipop.dao.CategoriaDAO;
import batoipop.dao.UsuarioDAO;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class GesCategoriasController implements Initializable {

    @FXML
    private ImageView imgBack;

    @FXML
    private TableView<Categoria> tblCategorias;

    @FXML
    private TableColumn<Categoria, Integer> clmnId;
    @FXML
    private TableColumn<Categoria, String> clmnNombre;

    private CategoriaDAO categoria = new CategoriaDAO();

    private Categoria reg = new Categoria();

    ObservableList<Categoria> obList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        Image imageBack = new Image("file:img/backarrow.png");
        imgBack.setImage(imageBack);

        try {
            visualizarTablas();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void goBack(MouseEvent event) throws IOException {
        Parent blah = FXMLLoader.load(getClass().getResource("menu.fxml"));
        Scene scene = new Scene(blah);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setTitle("BatoiPop");
        stage.show();
    }

    public void visualizarTablas() throws Exception {

        List<Categoria> registro = new ArrayList<>();
        registro = categoria.findAll();


        obList = FXCollections.observableArrayList(registro);

        if(obList.size() != 0){
            tblCategorias.setItems(obList);
            tblCategorias.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

            clmnId.setCellValueFactory(new PropertyValueFactory<>("id"));
            clmnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        }
    }

    @FXML
    private void addCategoria(){
        try {

            String mensaje = "Busqueda por cliente ID ";

            TextInputDialog dialog = new TextInputDialog("");


            dialog.setTitle("Buscar por ID");
            dialog.setHeaderText("");
            dialog.setContentText("ID:");

            Optional<String> result = dialog.showAndWait();

            result.ifPresent(name -> {
                try {
                    Categoria c = categoria.findByPK(Integer.parseInt(result.get()));
                    Alert alert = new Alert(Alert.AlertType.INFORMATION, mensaje, ButtonType.APPLY);
                    alert.setTitle("Cuidado!");
                    alert.setHeaderText(mensaje);
                    alert.setContentText("");
                    alert.showAndWait();
                } catch (Exception e) {
                    Alert alert = new Alert(Alert.AlertType.ERROR, mensaje, ButtonType.APPLY);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error");
                    alert.setContentText("No existe ese cliente");
                    alert.showAndWait();
                }

            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void updateItem(ActionEvent event) {
        try {
            String newName = "";
            TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Modificar nombre");
            dialog.setHeaderText(null);
            dialog.setContentText("Escribe el nombre de la nueva etiqueta:");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()){
                newName = result.get();
            }

            Categoria et = new Categoria(tblCategorias.getSelectionModel().getSelectedItem().getId(),newName);
            categoria.update(et);
            int indice = tblCategorias.getSelectionModel().getSelectedIndex();
            Categoria item = categoria.findByPK(tblCategorias.getSelectionModel().getSelectedItem().getId());
            obList.set(indice, item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package batoipop;

public class Empleado {

    private int id;
    private String nombre;
    private String apellidos;
    private String email;
    private String contraseña;

    public Empleado() {
    }

    public Empleado(int id, String nombre, String apellidos, String email, String contraseña) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.contraseña = contraseña;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getContraseña() {
        return contraseña;
    }
    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public String toString() {
        return "Empleado [id= " + id +
                ", nombre= " + nombre +
                ", apellidos= " + apellidos +
                ", email= " + email + '\'' +
                ", contraseña= " + contraseña + "]";
    }
}

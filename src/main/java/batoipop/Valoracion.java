package batoipop;

public class Valoracion {

    public int id;
    public int usuarioId;
    public int estrellas;

    public Valoracion() {
    }

    public Valoracion(int id, int usuarioId, int estrellas) {
        this.id = id;
        this.usuarioId = usuarioId;
        this.estrellas = estrellas;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getUsuarioId() {
        return usuarioId;
    }
    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getEstrellas() {
        return estrellas;
    }
    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

}

package batoipop;

public class Mensaje {

    private int id;
    private int usuarioEmisor;
    private int usuarioReceptor;
    private String texto;

    public Mensaje() {
    }

    public Mensaje(int id, int usuarioEmisor, int usuarioReceptor, String texto) {
        this.id = id;
        this.usuarioEmisor = usuarioEmisor;
        this.usuarioReceptor = usuarioReceptor;
        this.texto = texto;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getUsuarioEmisor() {
        return usuarioEmisor;
    }
    public void setUsuarioEmisor(int usuarioEmisor) {
        this.usuarioEmisor = usuarioEmisor;
    }

    public int getUsuarioReceptor() {
        return usuarioReceptor;
    }
    public void setUsuarioReceptor(int usuarioReceptor) {
        this.usuarioReceptor = usuarioReceptor;
    }

    public String getTexto() {
        return texto;
    }
    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public String toString() {
        return "Mensaje [id= " + id +
                ", usuarioEmisor= " + usuarioEmisor +
                ", usuarioReceptor= " + usuarioReceptor +
                ", texto= " + texto + "]";
    }
}
